Media Youtube Popcornjs lets you put deep linking (also referred to as chapters, cue points, clickable agendas) into Youtube videos. For full documentation see the project page on drupal.org.

Required Modules

    - media_youtube
    - popcornjs and its library popcorn-complete.min.js
    - cue_field, depending on field_collection, hms_field
    - cue_field_popcornjs
    - media_youtube_popcornjs

Setup

    - install all modules listed above as you normally would
    - install popcornjs library (read installation instructions for popcornjs module)
    - rename popcorn-complete.min.js to popcorn.min.js (v1.4 has better YouTube playback at this time)
    - add cue_field to video file types under structure > file types > manage fields
    - go to admin/structure/file-types/manage/video/fields
    - click "Add cue field collection" on action bar
    - provide the label and machine name for the field, let's assume it is called field_chapters as example
    - change the newly added field_chapters widget to Embedded
    - set up your field display if you are using together with video file directly (under manage display at structure > file types > video or admin/structure/file-types/manage/video/display), for views and integration with file display through a content type/entity see further down this page
    - change field_chapters format to "Cue point links for Popcorn.js"
    
Display via Views

field_chapters can be output using views and then applied to your theme via blocks, etc:
        - create a content block views, e.g. show_chapters, Show: Fields
        - add contexual filters "Content: Nid", configure, then choose "Provide default value": Content ID from URL
        - add relationships to your content type video file field, let's say you have a content type that has field_video, it will be "field_video:fid"
        - add field field_chapters available via the file relationships, set Formatter to "Cue point links for Popcorn.js"

        Due to a bug, the view will always output as long as there is video file attached to the node. To resolve this, you can implement hook_views_post_execute() to remove field collection with empty cue points:

        /**
        * Implements hook_views_post_execute().
        */
        function YOURMODULE_views_post_execute(&$view) {
          if ($view->name == 'show_chapters' && $view->current_display == 'block') {
            // removed field collection with empty cue points,
            // as file is counted as result even when cue points is empty
            $results =& $view->result;

            foreach ($results as $key => $result) {
              if (empty($result->field_chapters)) {
                unset($results[$key]);
              }
            }
          }
        }
        
        - add the views block to node page

Adding Chapters

    To add chapters (field_chapters) to video:
        - edit your video file under admin/content/file, you should see field_chapters field collection
        - Alternatively edit your video file via contexual link when viewing the video node

Known Issues

    There is a known issue with popcorn JS that currently makes this integration not work on ios7. You can follow it here https://github.com/mozilla/popcorn-js/issues/320, which ultimately will lead you to the reported issue to mozilla here: https://bugzilla.mozilla.org/show_bug.cgi?id=920103

    One way to get around this is to use Context module if you are serving a different theme to your mobile devices, and not include the blocks or code related to this feature.
