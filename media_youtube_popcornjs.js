(function ($) {

  /**
   * Attaches a seek event to each cue point link for Popcorn.js.
   */
  Drupal.behaviors.mediaYoutubePopcornjs = {
    attach: function(context, settings) {
      $.each(settings.mediaYoutubePopcornjs || {}, function(index, options) {
        // process all media youtube cue field
        $('#media-youtube-popcornjs-' + options.id, context).once('mediaYoutubePopcornjs', function() {
          // initialize popcorn youtube
          var video = $(this);
          var popcorn = Popcorn.smart('#media-youtube-popcornjs-' + options.id, options.url);
          options.popcorn = popcorn;
        });

        // attah cue field to popcorn youtube
        $('[data-cfpopcorn-id*="'+ options.id + '"]', context).once('mediaYoutubePopcornjs', function() {
          var cue = $(this);

          // jump to the cue point on click
          cue.on('click', function(e) {
            e.preventDefault();
            var start = cue.data('cfpopcorn-start');
            var p = options.popcorn;

            // do the jump & play
            p.currentTime(start).play();
          });
        });

      });
    }
  }

})(jQuery);
