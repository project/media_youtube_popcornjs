<?php

/**
 * @file media_youtube/includes/themes/media-youtube-video.tpl.php
 *
 * Template file for theme('media_youtube_video').
 *
 * Variables available:
 *  $uri - The media uri for the YouTube video (e.g., youtube://v/xsy7x8c9).
 *  $video_id - The unique identifier of the YouTube video (e.g., xsy7x8c9).
 *  $id - The file entity ID (fid).
 *  $url - The full url including query options for the Youtube iframe.
 *  $options - An array containing the Media Youtube formatter options.
 *  $api_id_attribute - An id attribute if the Javascript API is enabled;
 *  otherwise NULL.
 *  $width - The width value set in Media: Youtube file display options.
 *  $height - The height value set in Media: Youtube file display options.
 *  $title - The Media: YouTube file's title.
 *  $alternative_content - Text to display for browsers that don't support
 *  iframes.
 *
 *  $popcorn_fid - the file object id
 *  $popcorn_id - the html id in the form of 'media-youtube-popcornjs-ID',
 *  where ID = $popcorn_fid
 */
?>
<div id="<?php print $popcorn_id; ?>" class="<?php print $classes; ?> media-youtube-<?php print $id; ?>" style="width:<?php print $width; ?>px; height:<?php print $height; ?>px">
</div>
